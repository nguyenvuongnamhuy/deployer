#!/bin/sh

set -a
. ./.env
set +a

${UVICORN_PATH} main:app --host 0.0.0.0 --port 5050