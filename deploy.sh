#!/bin/bash

set -a
. ./.env
set +a

set -e
COMMIT_ID=$1
SOURCE_PATH=$2
SERVICE=$3
ENV=$4
LOG_PATH=$5

exec 3>&1 1>>${LOG_PATH} 2>&1

echo "Executing \`deploy.${SERVICE}.sh\`"
echo "Deploying \`${ENV}\` \`${SERVICE}\` with commit \`${COMMIT_ID}\`"

echo "$SOURCE_PATH"
cd $SOURCE_PATH

echo "$PWD"

echo "$ sudo git checkout ."
sudo git checkout .

echo "$ sudo git fetch"
sudo git fetch

# Deployment process
if [[ $ENV != "development" && $ENV != "release" ]]
then
  echo "$ sudo git checkout ${COMMIT_ID}"
  sudo git checkout $COMMIT_ID

  echo "$ sudo cp .env.${ENV} .env"
  sudo cp .env.${ENV} .env

  # export $(grep VERSION_ID .env)
  echo "$ sudo sed -i -e "s/VERSION_ID=/VERSION_ID=${COMMIT_ID}/g" .env"
  sudo sed -i -e "s/VERSION_ID=/VERSION_ID=${COMMIT_ID}/g" .env

  if [[ $SERVICE = "portal" ]]
  then
    export REACT_APP_VERSION_ID=${COMMIT_ID}
    docker compose up -d
    docker exec -i blink-portal-${ENV} ./scripts/build.sh
    docker compose restart
    cd ../api
    docker compose restart
  else
    export VERSION_ID=${COMMIT_ID}
    sudo chmod +x build.sh
    ./build.sh
  fi

  echo "\`${ENV}\` \`${SERVICE}\` deployed to Commit \`${COMMIT_ID}\`"
  echo "-------------------------------------------------------------"
fi