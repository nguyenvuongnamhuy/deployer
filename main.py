from subprocess import call
from time import sleep
from typing import Optional

from fastapi import BackgroundTasks, FastAPI, Form, Path, Request, status
from fastapi.responses import JSONResponse

from config import settings

app = FastAPI()

envs = ["development", "test", "release", "staging", "production"]

# @app.middleware("http")
# async def validate_header_authorization(
#     request: Request,
#     call_next,
# ):
#     if not (
#         settings.token == request.headers.get("authorization")
#         or settings.webhook_token == request.headers.get("X-Gitlab-Token")
#     ):
#         print("request.headers:", request.headers)
#         return JSONResponse(status_code=status.HTTP_401_UNAUTHORIZED, content="Invalid token")

#     return await call_next(request)


@app.get("/")
async def root():
    return "<h1>BLink Pipeline</h1>"


def call_process(commit_id: str, source_path: str, service: str, env: str, log_path: str):
    sleep(1)
    call(
        f"./deploy.sh {commit_id} {source_path} {service} {env} {log_path}",
        shell=True,
    )
    print(f"End deployment `{service}` with commit `{commit_id}", flush=True)


@app.post("/pipeline/{env}/{service}")
async def pipeline_trigger(
    background_tasks: BackgroundTasks,
    env: str = Path(
        title="System's Environment",
        regex=f"^({'|'.join(envs)})$",
    ),
    service: str = Path(
        title="System's Service",
        regex=r"^(api|portal)$",
    ),
    commit_id: str = Form(),
    triggerer: Optional[str] = Form(),
):
    if triggerer and settings.authorized_users and triggerer not in settings.authorized_users.split(","):
        return JSONResponse(
            status_code=status.HTTP_401_UNAUTHORIZED,
            content=f"Invalid Triggerer, only these users can be authorized: `{settings.authorized_users}`",
        )

    print(f"\nTriggering `{env}` `{service}` with commit `{commit_id}`", flush=True)
    background_tasks.add_task(
        call_process, commit_id, f"{settings.root_path}/{service}", service, env, settings.log_path
    )
    return f"Received deployment {commit_id}. The server is deploying. Please wait for a minute"


@app.post("/pipeline/webhook")
async def webhook_trigger(
    request: Request,
    background_tasks: BackgroundTasks,
):
    refs = {
        "refs/heads/develop": "development",
        "refs/heads/test": "test",
    }
    payload = await request.json()
    if not (payload["object_kind"] == payload["event_name"] == "push" and payload["ref"] in refs):
        return JSONResponse(status_code=status.HTTP_503_SERVICE_UNAVAILABLE, content="Sorry, not available")

    return JSONResponse(status_code=status.HTTP_503_SERVICE_UNAVAILABLE, content="Sorry, not available yet")

    # TODO: Seperate dev & test for clv-server only
    env = refs[payload["ref"]]
    commit_id = payload["checkout_sha"][:8]
    service = payload["project"]["homepage"].split("/")[-1]
    print(f"\nTriggering `{env}` `{service}` with commit `{commit_id}`", flush=True)
    background_tasks.add_task(
        call_process, commit_id, f"{settings.root_path}/{service}", service, env, settings.log_path
    )
    return "Deploying"