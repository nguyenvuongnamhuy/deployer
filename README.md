# Deployer Service

## Overall

Deployer service aim to speed up deployment automatically by CI/CD based on webhook mechanic

## Pre-requirements

- python 3.6
- node 14 (install via nvm)
- pm2 (
  [Install](https://pm2.keymetrics.io/docs/usage/quick-start) /
  [Share the same daemon process](https://sobus-piotr.medium.com/pm2-share-the-same-daemon-process-between-multiple-users-dd7ecae6197a)
  )