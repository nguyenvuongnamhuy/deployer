from typing import Optional

from pydantic import BaseSettings


class Settings(BaseSettings):
    token: str
    webhook_token: str

    root_path: str
    log_path: str

    authorized_users: Optional[str]

    class Config:
        env_file = ".env"


settings = Settings()